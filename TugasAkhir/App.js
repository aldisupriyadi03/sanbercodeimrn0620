import React from "react";
import Page from "./Page";
import { Provider } from "react-redux";
import { store } from "./Redux/";

export default function App() {
  return (
    <Provider store={store}>
      <Page />
    </Provider>
  );
}
