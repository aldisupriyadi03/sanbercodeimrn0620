import React, { useEffect } from "react";
import Ilustrator from "../assets/Register.png";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
} from "react-native";

import { useSelector, useDispatch } from "react-redux";
import { setForm } from "../Redux/action";

const Register = (props) => {
  const state = useSelector((state) => state.RegisterReducer);
  const dispatch = useDispatch();

  const login = () => {
    props.navigation.push("Home");
  };

  const onInputChange = (value, name) => {
    dispatch(setForm(name, value));
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Register</Text>
      <Image style={styles.image} source={Ilustrator} />

      {
        //Input Container
      }
      <View style={styles.inputContainer}>
        <View style={{ marginBottom: 20 }}>
          <TextInput
            style={styles.textInput}
            placeholder="Name"
            value={state.form.name}
            onChangeText={(value) => onInputChange(value, "name")}
          />
        </View>
        <View style={{ marginBottom: 20 }}>
          <TextInput
            style={styles.textInput}
            value={state.form.email}
            onChangeText={(value) => onInputChange(value, "email")}
            placeholder="Email"
          />
        </View>
        <View style={{ marginBottom: 20 }}>
          <TextInput
            style={styles.textInput}
            value={state.form.password}
            onChangeText={(value) => onInputChange(value, "password")}
            placeholder="Password"
          />
        </View>
        <View style={{ marginBottom: 10 }}>
          <TextInput
            style={styles.textInput}
            value={state.form.repassword}
            onChangeText={(value) => onInputChange(value, "repassword")}
            placeholder="RePassword"
          />
        </View>
      </View>

      <Text style={{ marginLeft: 35, marginRight: 20 }}>
        By signing up, you agree to
        <Text style={{ fontStyle: "italic", color: "#000000" }}>
          {" "}
          Terms of Service{" "}
        </Text>
        and
        <Text style={{ fontStyle: "italic", color: "#000000" }}>
          {" "}
          Privacy Policy.
        </Text>
      </Text>
      {
        //Button Container
      }
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.buttonLogin}>
          <Text style={styles.buttonText} onPress={() => login()}>
            NEXT
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  titleText: {
    marginBottom: 28,
    marginRight: 280,
    color: "#000000",
    lineHeight: 27,
    margin: 20,
    fontFamily: "Comfortaa",
    fontSize: 24,
    textAlign: "left",
  },
  image: {
    marginLeft: 28,
    width: 348,
    height: 241,
  },
  inputContainer: {
    // flexDirection: "row",
    marginBottom: 5,
  },
  textInput: {
    fontFamily: "Roboto",
    width: 343,
    fontSize: 20,
    paddingLeft: 20,
    height: 52,
    marginLeft: 28,
    borderRadius: 10,
    borderWidth: 2,
    borderStyle: "solid",
  },
  buttonContainer: {
    marginTop: 20,
    marginLeft: 28,
  },
  buttonLogin: {
    borderRadius: 6,
    width: 343,
    height: 52,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#000000",
  },
  buttonText: {
    fontFamily: "Roboto",
    fontSize: 20,

    color: "#FFFFFF",
  },
  buttonRegister: {
    borderRadius: 6,
    borderWidth: 2,
    borderStyle: "solid",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5F5F5",
    width: 343,
    height: 52,
    marginTop: 26,
  },
  textRegister: {
    fontFamily: "Roboto",
    fontSize: 20,
    color: "#000000",
  },
});

export default Register;
