import React, { useState, useEffect } from "react";
import fetch from "node-fetch";
import {
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { ListItem, SearchBar, Header } from "react-native-elements";
import _ from "lodash";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const Home = (props) => {
  const [state, setstate] = useState({
    item: [],
    fullitem: [],
    search: "",
    isLoading: true,
  });

  const contains = (item, query) => {
    const { company, title, location } = item;
    if (
      company.includes(query) ||
      title.includes(query) ||
      location.includes(query)
    ) {
      return true;
    }

    return false;
  };

  const updateSearch = (search) => {
    if (search.length === 0) {
      setstate({ search, item: state.fullitem, fullitem: state.fullitem });
    } else {
      const filtered = _.filter(state.fullitem, (job) => {
        return contains(job, search);
      });

      setstate({ search, item: filtered, fullitem: state.fullitem });
    }
  };

  const getData = async () => {
    const item = await fetch(
      "https://jobs.github.com/positions.json"
    ).then((res) => res.json());
    setstate({ item, fullitem: item, ActivityIndicator: false });
  };

  useEffect(() => {
    getData();
  }, []);

  const renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() => props.navigation.navigate("Detail", { item })}
    >
      <ListItem
        title={item.title}
        subtitle={item.location}
        leftAvatar={{
          source: item.company_logo && { uri: item.company_logo },
          title: item.company,
        }}
        bottomDivider
        chevron
      />
    </TouchableOpacity>
  );

  const HeaderRightComponent = () => {
    return (
      <TouchableOpacity>
        <Icon
          name="account-circle"
          color="#fff"
          size={30}
          onPress={() => props.navigation.navigate("About")}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Header
        backgroundColor="grey"
        centerComponent={{ text: "Github Job Desk", style: { color: "#fff" } }}
        rightComponent={<HeaderRightComponent />}
      />
      <SearchBar
        onChangeText={(value) => updateSearch(value)}
        value={state.search}
        placeholder="Search Job..."
        lightTheme
        round
      />

      {state.isLoading && (
        <View style={{ justifyContent: "center", marginTop: 200 }}>
          <ActivityIndicator size="large" color="grey" />
        </View>
      )}

      <FlatList
        keyExtractor={(item, index) => index.toString()}
        data={state.item}
        renderItem={renderItem}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  navbar: {
    alignItems: "center",
    height: 100,
    borderBottomStartRadius: 50,
    borderBottomEndRadius: 50,
    backgroundColor: "#F5F5F5",
  },
  textNavbar: {
    marginTop: 5,
    fontSize: 25,
    color: "#000000",
  },
  listdata: {
    marginBottom: 30,
    backgroundColor: "#f5f6f6",
    height: 100,
  },
});

export default Home;
