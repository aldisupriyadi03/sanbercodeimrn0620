import React from "react";
import { View, StyleSheet, TouchableOpacity, Linking } from "react-native";
import { Header, Avatar, Text } from "react-native-elements";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useSelector } from "react-redux";

const About = () => {
  const AboutReducer = useSelector((state) => state.AboutReducer);

  return (
    <View style={styles.container}>
      <Header
        backgroundColor={"grey"}
        centerComponent={{ text: "About Me", style: { color: "#fff" } }}
      />
      <View style={{ alignItems: "center", marginTop: 30 }}>
        <Avatar
          rounded
          size="xlarge"
          source={{
            uri:
              "https://pbs.twimg.com/profile_images/378800000244149803/aaf0805a8a63aa3cc2daa80b6954c74e.jpeg",
          }}
        />
        <Text h4 style={{ marginTop: 20, color: "#FF0000" }}>
          {AboutReducer.name}
        </Text>
        <Text h5 style={{ color: "#0C0C0C", marginBottom: 10 }}>
          {AboutReducer.location}
        </Text>
      </View>

      <View>
        <Text
          style={{
            fontSize: 20,
            marginTop: 5,
            color: "#FF0000",
            fontStyle: "italic",
            marginLeft: 15,
          }}
        >
          Profil Saya
        </Text>
        <Text
          h5
          style={{
            marginTop: 15,
            marginLeft: 15,
            marginRight: 10,
            color: "#FF0000",
          }}
        >
          {AboutReducer.AboutText}
        </Text>
      </View>

      <View style={{ alignItems: "center" }}>
        <Text
          style={{
            fontSize: 20,
            marginTop: 180,

            color: "#FF0000",
            fontStyle: "italic",
          }}
        >
          Contact Me
        </Text>
      </View>

      <View
        style={{
          marginTop: 20,
          justifyContent: "space-evenly",
          flexDirection: "row",
        }}
      >
        <TouchableOpacity
          onPress={() => Linking.openURL(AboutReducer.Facebook)}
        >
          <Icon name="facebook" size={40} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => Linking.openURL(AboutReducer.Instagram)}
        >
          <Icon name="instagram" size={40} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => Linking.openURL(AboutReducer.Whatsapp)}
        >
          <Icon name="whatsapp" size={40} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
});

export default About;
