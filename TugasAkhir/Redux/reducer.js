import { combineReducers } from "redux";

const AboutState = {
  name: "Aldi Supriyadi",
  location: "Jakarta Timur",
  AboutText:
    "Saya adalah lulusan S1 tekhnik informatika, yang sedang belajar mendalami pembuatan aplikasi android.",
  Facebook: "https://www.facebook.com/aldi.supriyadi1/",
  Instagram: "https://instagram.com/aldiyaa_",
  Whatsapp: "https://api.whatsapp.com/send?phone=6281318601518",
};

const RegisterState = {
  form: {
    name: "",
    email: "",
    password: "",
    repassword: "",
  },
};

const LoginState = {
  form: {
    email: "",
    password: "",
  },
  isWrongPass: false,
};

const RegisterReducer = (state = RegisterState, action) => {
  if ((action.type = "SET_FORM")) {
    return {
      ...state,
      form: {
        ...state.form,
        [action.inputType]: action.inputValue,
      },
    };
  }
  return state;
};

const AboutReducer = (state = AboutState, action) => {
  return state;
};

const LoginReducer = (state = LoginState, action) => {
  if ((action.type = "SET_FORM")) {
    return {
      ...state,
      form: {
        ...state.form,
        [action.inputType]: action.inputValue,
      },
    };
  }
  return state;
};

const reducer = combineReducers({
  RegisterReducer,
  AboutReducer,
  LoginReducer,
});

export default reducer;
