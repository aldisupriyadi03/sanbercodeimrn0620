//soal1
function arrayToObject(arr) {
    for (a=0; a<arr.length; a++){
        yaha = arr[a];
        tanggal = new Date();
        thisYear = tanggal.getFullYear();
        var umur;
        if(yaha[3]==undefined || yaha[3]>thisYear){
            umur = "Invalid Birth Year";
        }else{
            umur = thisYear-yaha[3];
        }
        obj = {
            firstName : yaha[0],
            lastName : yaha[1],
            gender: yaha[2],
            umur : umur
        }
        json = JSON.stringify(obj);
        console.log(a+1+". "+yaha[0]+" "+yaha[1]+": "+json);
    }
    
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people);
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2);
arrayToObject([]);





//soal2
function shoppingTime(memberId, money) {
    let ret ;
    if (memberId==undefined || memberId==''){
        ret = "Mohon maaf, toko X hanya berlaku untuk member saja";
        return ret;
    }if(money<50000 || money==undefined){
        ret = "Mohon maaf, uang tidak cukup";
       return ret;
    }
    let list = [];
    let kembalian = money;
    if(kembalian >= 1500000){
        kembalian = kembalian - 1500000;
        list.push("Sepatu Stacattu");
    } 
        if(kembalian >= 500000){
            kembalian = kembalian - 500000;
            list.push("Baju Zoro");
        } 
            if(kembalian >= 250000){
            kembalian = kembalian - 250000;
            list.push("Baju H&N");
            } 
                if(kembalian >= 175000){
                kembalian = kembalian - 175000;
                list.push("Sweater Uniklooh");
                } 
                    if(kembalian >= 50000){
                    kembalian = kembalian - 50000;
                    list.push("Casing Handphone");
                    }
                        let obj2 = {
                        memberId : memberId,
                        money : money,
                        listPurchased : list,
                        changeMoney : kembalian
                        }
    return obj2;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


//soal3
function naikAngkot(arrPenumpang) {
    let ret2 = [];
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    tarif = ['1', '2', '2', '4', '5', '6'];
    for(a=0; a<arrPenumpang.length; a++){
        let y = arrPenumpang[a];
        let int = 0;
        let int2 = 0;
        for (z=0; z<rute.length; z++){
            if(y[1]==rute[z]){
                int = tarif[z];
            }
                if(y[2]==rute[z]){
                int2 = tarif[z];
                }
        }
        let rutetarif=int2-int;
        let obj3 = {
            penumpang : y[0],
            naikDari : y[1],
            tujuan : y[2],
            bayar : rutetarif*2000
        }
        ret2.push(obj3);
    }
    return ret2;
}
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));
