import React,{Component} from 'react';
import{
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList
} from 'react-native'
import Icon from "react-native-vector-icons/MaterialIcons";
import VideoItems from "./Components/videoItems";
const instruction=Platform.select({
    ios:"Press Cmd+R to reload,\n Cmd+D or shake for dev menu",
    android:"Double Tap R on your keyboard to reload,\n Shake or Press menu button for dev menu"
})
import data from "./data.json";
export default class App extends Component{
    render(){
        return <View style={styles.container}>
            <View style={styles.navbar}>
                <Image source={require('./Images/logo.png')} style={{width:98,height:22}}/>
                <View style={styles.rightNav}>
                    <TouchableOpacity> 
                        <Icon name="search" style={styles.navItems} size={25}></Icon>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name="account-circle" style={styles.navItems} size={25}></Icon>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.body}>
                <FlatList
                data={data.items}
                renderItem={(video)=><VideoItems video={video.item} index={video.index}/>}
                keyExtractor={(video)=>video.id}
                ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}></View>}
                ></FlatList>
            </View>
            <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem }>
                        <Icon name="home" size={25}></Icon>
                        <Text>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem }>
                        <Icon name="whatshot" size={25}></Icon>
                        <Text>Trending</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem }>
                        <Icon name="subscriptions" size={25}></Icon>
                        <Text>Subscriptions</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem }>
                        <Icon name="folder" size={25}></Icon>
                        <Text>Libraries</Text>
                    </TouchableOpacity>
            </View>
        </View>
    }
}

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    body:{
        flex:1
    },
    tabTitle:{
        fontSize:11,
        color:'#3c3c3c'
    },
    tabItem:{
        justifyContent:'center',
        alignItems:'center'
    },
    tabBar:{
        height:60,
        flexDirection:'row',
        backgroundColor:'white',
        borderTopWidth:0.5,
        borderColor:'#E5E5E5',
        elevation:10,
        justifyContent:'space-around',
        alignItems:'center'
    },
    navItems:{
        marginLeft:25
    },
    rightNav:{
        flexDirection:'row',
    },
    navbar:{
        height:55,
        marginTop:5,
        elevation:3,
        backgroundColor:'white',
        paddingHorizontal:15,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    }
})
// export default registerRootComponent(App);