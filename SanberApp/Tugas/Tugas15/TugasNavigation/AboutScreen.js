import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Tentang Saya</Text>
        <View style={{ alignItems: "center" }}>
          <Icon name="md-person" size={250} />
        </View>
        <Text style={styles.name}>Rafli Rachmawandi</Text>
        <Text style={styles.job}>React Native Developer</Text>
        <View style={styles.folioContainer}>
          <Text style={styles.folioTitle}>Portofolio</Text>
          <View style={styles.folioBody}>
            <TouchableOpacity style={styles.folioItem}>
              <Icon name="logo-github" size={50} color={"#3EC6FF"} />
              <Text style={{ color: "#003366", fontWeight: "bold" }}>
                @raflirach
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.folioItem}>
              <Icon name="logo-github" size={50} color={"#3EC6FF"} />
              <Text style={{ color: "#003366", fontWeight: "bold" }}>
                @raflirach
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.folioContainer}>
          <Text style={styles.folioTitle}>Portofolio</Text>
          <View style={styles.aboutBody}>
            <TouchableOpacity style={styles.aboutItem}>
              <Icon name="logo-facebook" size={50} color={"#3EC6FF"} />
              <Text
                style={{ color: "#003366", fontWeight: "bold", marginLeft: 10 }}
              >
                rafli.tzie
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.aboutItem}>
              <Icon name="logo-instagram" size={50} color={"#3EC6FF"} />
              <Text
                style={{ color: "#003366", fontWeight: "bold", marginLeft: 10 }}
              >
                @rafli_rach
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.aboutItem}>
              <Icon name="logo-twitter" size={50} color={"#3EC6FF"} />
              <Text
                style={{ color: "#003366", fontWeight: "bold", marginLeft: 10 }}
              >
                @madskiba1
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: "white",
  },
  title: {
    textAlign: "center",
    fontSize: 32,
    color: "#003366",
    fontWeight: "bold",
  },
  name: {
    textAlign: "center",
    fontSize: 24,
    color: "#003366",
    fontWeight: "bold",
  },
  job: {
    textAlign: "center",
    fontSize: 18,
    color: "#3EC6FF",
    marginBottom: 20,
  },
  folioContainer: {
    backgroundColor: "#efefef",
    marginHorizontal: 10,
    borderRadius: 8,
    padding: 2,
    marginVertical: 5,
  },
  folioTitle: {
    borderBottomWidth: 1,
    borderBottomColor: "#003366",
  },
  formContainer: {
    marginVertical: 20,
    alignItems: "center",
  },
  folioBody: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 30,
    paddingVertical: 10,
  },
  folioItem: {
    flexDirection: "column",
    alignItems: "center",
  },
  aboutBody: {
    flexDirection: "column",
    alignItems: "center",
  },
  aboutItem: {
    flexDirection: "row",
    alignItems: "center",
  },
});
