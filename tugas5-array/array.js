//soal1
function range (num1, num2) {
    var numbers = [];
    if(typeof num1!=='undefined' && typeof num2!=='undefined') {
        if(num1>num2) {
            for(var i=num1; i>=num2; i--) {
                numbers.push(i);
            }
        } else {
            for(var i=num1; i<=num2; i++) {
                numbers.push(i);
            }
        }
    } else {
        numbers.push(-1);
    }

    return numbers;
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

//soal2
function rangeWithStep (num1, num2, step) {
    var numbers = [];
    if(num1>num2) {
        for(var i=num1; i>=num2; i-=step) {
            numbers.push(i);
        }
    } else {
        for(var i=num1; i<=num2; i+=step) {
            numbers.push(i);
        }
    }

    return numbers;
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))


//soal3
function sum (num1, num2, step) {
    var numbers = [];
    if(typeof num1!=='undefined' && typeof num2!=='undefined' && typeof step!=='undefined') {
        numbers = rangeWithStep(num1, num2, step);
    } else if(typeof num1!=='undefined' && typeof num2!=='undefined') {
        numbers = range(num1, num2);
    } else if(typeof num1!=='undefined') {
        numbers.push(num1);
    } else {
        numbers.push(0);
    }
    var result = 0;
    for(var i=0; i<numbers.length; i++){
        result+=numbers[i];
    }
    return result;
}
console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())


//soal4
function dataHandling(input) {
    for (var i=0; i < input.length; i++){
        console.log("Nomor ID: "+input[i][0]);
        console.log("Nama Lengkap: "+input[i][1]);
        console.log("TTL: "+input[i][2]+" "+input[i][3]);
        console.log("Hobi: "+input[i][4]);
        console.log("");
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] ;
dataHandling(input);

//soal5
function balikKata(teks) {
    teks_terbalik = '';
    for(var i=teks.length-1; i>=0; i--) {
        teks_terbalik += teks[i];
    }
    return teks_terbalik;
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))


//soal6
function konversiBulan(bulan) {
    var bulanString = '';
    switch (bulan) {
        case '01': { 
            bulanString = 'Januari';
            break;
        }
        case '02': { 
            bulanString = 'Februari';
            break;
        }
        case '03': { 
            bulanString = 'Maret';
            break;
        }
        case '04': { 
            bulanString = 'April';
            break;
        }
        case '05': { 
            bulanString = 'Mei';
            break;
        }
        case '06': { 
            bulanString = 'Juni';
            break;
        }
        case '07': { 
            bulanString = 'Juli';
            break;
        }
        case '08': { 
            bulanString = 'Agustus';
            break;
        }
        case '09': { 
            bulanString = 'September';
            break;
        }
        case '10': { 
            bulanString = 'Oktober';
            break;
        }
        case '11': { 
            bulanString = 'November';
            break;
        }
        case '12': { 
            bulanString = 'Desember';
            break;
        }
    }
    return bulanString;
}
function dataHandling2(input) {
    input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    input.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(input);

    var bulan = '';
    bulan = input[3].split("/");
    console.log(konversiBulan(bulan[1]));
    
    var bulan_desc = input[3].split("/");
    bulan_desc.sort(function (value1, value2) { return value2-value1 } );
    console.log(bulan_desc);

    console.log(bulan.join("-"));
 
    console.log(input[1].slice(0,15));
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

